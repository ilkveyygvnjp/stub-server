package com.stub.stub.controller;


import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StubController implements com.stub.stub.controller.UserApi {
    @RequestMapping(path = "/login", method ={ RequestMethod.POST })
    public String authentication(@RequestBody String body) {
        return  "This is an Authentication Application";
    }
}
